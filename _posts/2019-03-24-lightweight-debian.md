---
layout:       post
title:        Lightweight Debian installation
description:  Installing a lightweight and minimal Debian system
lang:         en
ref:          lightweight-debian
categories:   blog
short:        na56
tags:         [linux, unix, debian, debootstrap, netboot, mini, chroot, qemu ]
---

{% include toc.md %}

It has often been said that some distributions are *[bloated][3]* or not as
[minimal][16] as others like [Arch][2], [Gentoo][11], [Void][25] or
[OpenBSD][19].

Even though most major distributions, either BSD or Linux, can't really be
considered *bloated*, it can be argued that some desktop-oriented setups or
*[flavors][10]* are far from minimal in their default configurations.

Having said that, nothing really stops a user from making a bare bones install
that will take very few resources. This principle applies to most major
distributions, which often provide minimal installation processes where the
system can be tailored to the user's needs.

In this post, we will try to get a lightweight system using [Debian][5]'s
readily minimal installation options.

<!--more-->

## Installing the base system

### Doing a *netboot* install

The easiest way to get a minimal Debian installation is to use the `mini.iso`
file, commonly known as *[netboot][17]* (a portmanteau of *network boot*). We
can download the latest ISO from Debian's website:

```terminal
wget https://deb.debian.org/debian/dists/stretch/main/installer-amd64/current/images/netboot/mini.iso
```

We will be using *[QEMU][20]* to do the installation, so we pass both the ISO
file and the destination device (e.g. `/dev/sdi`) as arguments to the
executable:

```terminal
qemu-system-x86_64 -m 256 -drive file=mini.iso,media=cdrom -drive file=/dev/sdi,format=raw,cache=none
```

From here on we can use most of the defaults. We just have to make sure nothing
is selected during the Software selection step of the installation process:

{% include postImage.html file="software-selection.png" alt="Debian software
selection" width=800 height=600 %}


### Using *debootstrap*

An alternative option is to use the package *[debootstrap][6]* to create a bare
installation. To install the necessary packages in our local system, we run:

```terminal
sudo apt install debootstrap
```

Since there is no installer to help us create the partitions, we must create
them manually. We can use *[GParted][12]*, *[fdisk][9]* or any other
partitioning tool for this:

```terminal
printf 'o\nn\np\n1\n\n\nw\n' | sudo fdisk /dev/sdi
```

We must then create an *[ext4][8]* file system with the command:

```terminal
sudo mkfs.ext4 /dev/sdi1
mke2fs 1.43.4 (31-Jan-2017)
Creating filesystem with 61049389 4k blocks and 15269888 inodes
Filesystem UUID: 60bb0786-b320-4285-abc0-95efce9ac10b
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208,
        4096000, 7962624, 11239424, 20480000, 23887872

Allocating group tables: done
Writing inode tables: done
Creating journal (262144 blocks): done
Writing superblocks and filesystem accounting information: done
```

Now, let's mount the partition:

```terminal
sudo mount /dev/sdi1 /mnt/debian
```

And use *debootstrap* to copy the base system:

```terminal
sudo debootstrap --arch amd64 stretch /mnt/debian http://deb.debian.org/debian
```

This will take a few minutes since it has to download the required packages and
copy them on to the disk.


#### Configuring the system

Now we will *[chroot][4]* into our newly created system to apply some finishing
touches. But before doing this, we need to recreate some basic file system
hierarchy:

```terminal
sudo mount -t proc /proc /mnt/debian/proc && \
sudo mount --rbind --make-rslave /dev /mnt/debian/dev && \
sudo mount --rbind --make-rslave /sys /mnt/debian/sys && \
sudo mount --rbind --make-rslave /run /mnt/debian/run
```

Now we can safely use *chroot*:

```terminal
sudo chroot /mnt/debian /bin/bash
```

First of all, let's change the *hostname*:

```terminal
echo 'debianlight' > /etc/hostname && \
echo -e '127.0.1.1\tdebianlight' >> /etc/hosts
```
{: .superuser}

By default, `/etc/fstab` is empty, so we must add the disk with the *UUID* of
the file system we created earlier. We can use the command `blkid` to see it:

```terminal
blkid
/dev/sdi1: UUID="60bb0786-b320-4285-abc0-95efce9ac10b" TYPE="ext4" PARTUUID="17a30f04-01"
```
{: .superuser}

So let's create the appropriate entry:

```terminal
cat <<- 'EOF' > /etc/fstab
UUID=60bb0786-b320-4285-abc0-95efce9ac10b / ext4 defaults 0 0
EOF
```
{: .superuser}

For Debian's stable release, updates to stuff like virus scanners or timezone
data are delivered via the *[updates][24]* repository. Therefore, it's a good
idea to add it to our `sources.list`:

```terminal
cat <<- 'EOF' >> /etc/apt/sources.list
deb http://deb.debian.org/debian stretch-updates main
EOF
```
{: .superuser}

We should also make sure to enable the security updates, especially if this
system is to be online. For this, we need to add the repository for Debian's
[security team][21]:

```terminal
cat <<- 'EOF' >> /etc/apt/sources.list
deb http://security.debian.org/debian-security stretch/updates main
EOF
```
{: .superuser}

Let's get our system up to date:

```terminal
apt update && apt upgrade --no-install-recommends
```
{: .superuser}

A *debootstrap* installation is mainly used for *chroots* or *containers*,
therefore it's missing a few fundamental packages. To have a bootable system, we
must install them before we boot the machine for the first time.

First let's install and configure the *locales* to avoid some annoying error
messages:

```terminal
apt install --no-install-recommends locales && dpkg-reconfigure locales
```
{: .superuser}

Since no timezone is configured, the time and date may be reported erroneously.
Let's fix that:

```terminal
dpkg-reconfigure tzdata
```
{: .superuser}

Of course, we shouldn't forget about installing a kernel:

```terminal
apt install --no-install-recommends linux-image-amd64
```
{: .superuser}

Also, we have to install a boot loader on our disk (e.g. `/dev/sdi`) so the
system can be booted:

```terminal
apt install --no-install-recommends grub-pc && update-grub
```
{: .superuser}

And replace all entries in `grub.cfg` with the *UUID* for our disk:

```terminal
sed -i 's,root=/dev/sdi[0-9],root=UUID=60bb0786-b320-4285-abc0-95efce9ac10b,' /boot/grub/grub.cfg
```
{: .superuser}

At the moment, we are using our system's network connection inside the *chroot*,
but this won't be available once we boot this new system by itself. We need to
add a [configuration for our network card][14] in the
`/etc/network/interfaces.d` directory.  Since we are going to test the
installation with *QEMU*, we can use the default interface's name:

```terminal
cat <<- 'EOF' > /etc/network/interfaces.d/ens3
allow-hotplug ens3
iface ens3 inet dhcp
EOF
```
{: .superuser}

In order to login as *root*, we'll need to set a password:

```terminal
passwd
Enter new UNIX password:
Retype new UNIX password:
passwd: password updated successfully
```
{: .superuser}

Finally, let's clean up and exit the *chroot* so we can boot into our new
system:

```terminal
apt clean; exit
```
{: .superuser}

Although first we should cleanly unmount the file hierarchy we previously set
up:

```terminal
sudo umount -R /mnt/debian
```

## Booting the system

Now, let's boot up with *QEMU*:

```terminal
qemu-system-x86_64 -m 256 -drive file=/dev/sdi,format=raw,cache=none -net user,hostfwd=tcp::2222-:22 -net nic
```

Manually entering commands in *QEMU*'s console is not very efficient (it's not
possible to copy and paste), so it's always a good idea to install *[SSH][22]* so
we can access the system remotely. After logging in as *root*, we can install
it:

```terminal
apt install --no-install-recommends ssh
```
{: .superuser}

We must also enable *root* access by modifying `/etc/ssh/sshd_config`:

```terminal
cat <<- 'EOF' >> /etc/ssh/sshd_config
PermitRootLogin yes
EOF
```
{: .superuser}

Once we have rebooted the virtual machine, we should be able to connect from our
own machine using *SSH*:

```terminal
ssh -p 2222 root@localhost
```


### System resources usage

Let's see how much of the system's resources we are using. For instance, for the
*netboot* install:

```terminal
df -h --output=source,used,target /
Filesystem      Used Mounted on
/dev/sda1       635M /
```
{: .superuser}

```terminal
free -ht | grep ^Total
Total:  240M  26M 168M
```
{: .superuser}

```terminal
dpkg -l | grep ^ii | wc -l
217
```
{: .superuser}

And for the *debootstrap* install:

```terminal
df -h --output=source,used,target /
Filesystem      Used Mounted on
/dev/sda1       579M /
```
{: .superuser}

```terminal
free -ht | grep ^Total
Total:  240M  26M 169M
```
{: .superuser}

```terminal
dpkg -l | grep ^ii | wc -l
191
```
{: .superuser}

Not bad! But we can do better.


### Avoiding unnecessary packages

Since we are trying to make this system as minimal as possible, we should make
sure only the required packages are installed without having to provide the
`--no-install-recommends` option every time:

```terminal
cat <<- 'EOF' >> /etc/apt/apt.conf.d/99local
APT::Install-Suggests "0";
APT::Install-Recommends "0";
EOF
```
{: .superuser}


### Removing some fluff

Now, we can trim some disk space by deleting unused locales. We can just use a
one-liner to do this:

```terminal
find /usr/share/locale -mindepth 1 -maxdepth 1 ! -name 'en*' -exec rm -r {} \;
```
{: .superuser}

To prevent packages from installing unwanted locales, we can force *[dpkg][7]*
to ignore them:

```terminal
cat <<- 'EOF' > /etc/dpkg/dpkg.cfg.d/01_nolocales
path-exclude /usr/share/locale/*
path-include /usr/share/locale/en*
EOF
```
{: .superuser}

The same thing can be done for documentation files:

```terminal
find /usr/share/doc -depth -type f ! -name copyright -delete
```
{: .superuser}

```terminal
find /usr/share/doc -empty -delete
```
{: .superuser}

```terminal
rm -rf /usr/share/man /usr/share/groff /usr/share/info /usr/share/lintian /usr/share/linda /var/cache/man
```
{: .superuser}

And to prevent them from being installed at all:

```terminal
cat <<- 'EOF' > /etc/dpkg/dpkg.cfg.d/01_nodocs
path-exclude /usr/share/doc/*
path-include /usr/share/doc/*/copyright
path-exclude /usr/share/man/*
path-exclude /usr/share/groff/*
path-exclude /usr/share/info/*
path-exclude /usr/share/lintian/*
path-exclude /usr/share/linda/*
EOF
```
{: .superuser}

We can even get more space by removing some unnecessary packages:

```terminal
apt purge --auto-remove apt-listchanges aptitude aspell* at avahi-autoipd avahi-daemon bc bluetooth debconf-i18n debian-faq* doc-debian eject exim4-base groff iamerican ibritish info installation-report ispell* krb5-locales logrotate manpages modemmanager nano os-prober pcscd ppp popularity-contest reportbug rsyslog util-linux-locales wamerican
```
{: .superuser}

To remove old logs, we can run the following command:

```terminal
find /var/log -type f -cmin +10 -delete
```
{: .superuser}


## Final comparison

What does our system looks like after all these improvements? Let's see how the
*netboot* install fares:

```terminal
df -h --output=source,used,target /
Filesystem      Used Mounted on
/dev/sda1       544M /
```
{: .superuser}

```terminal
free -ht | grep ^Total
Total:  240M  25M 170M
```
{: .superuser}

```terminal
dpkg -l | grep ^ii | wc -l
202
```
{: .superuser}

And for the *debootstrap* install:

```terminal
df -h --output=source,used,target /
Filesystem      Used Mounted on
/dev/sda1       503M /
```
{: .superuser}

```terminal
free -ht | grep ^Total
Total:  240M  25M 171M
```
{: .superuser}

```terminal
dpkg -l | grep ^ii | wc -l
187
```
{: .superuser}

We can see that the *netboot* install adds some extra packages that we could
easily remove, although the improvement would be marginal:

```terminal
apt purge --auto-remove busybox discover kbd keyboard-configuration laptop-detect pciutils task-english
```
{: .superuser}


## Other tweaks

### Removing *systemd*

If we are no fans of *systemd* and aren't using any of its features, we can
[remove it][18] and install a different *init* system in its place. For
this example, we will install *[SysV][23]*:

```terminal
apt install --purge --auto-remove --no-install-recommends sysvinit-core
```
{: .superuser}

Then, let's create an `inittab` file:

```terminal
cp /usr/share/sysvinit/inittab /etc/inittab
```
{: .superuser}

After a reboot using the new *init system*, we can purge *systemd*:

```terminal
apt purge --auto-remove systemd libpam-systemd
```
{: .superuser}

To avoid installing any *systemd* package in the future, we will configure
*[APT][1]* accordingly:

```terminal
cat <<- 'EOF' >> /etc/apt/preferences.d/nosystemd
Package: libsystemd0
Pin: release *
Pin-Priority: 500

Package: *systemd*
Pin: release *
Pin-Priority: -1
EOF
```
{: .superuser}

Without *systemd*, we get a slight improvement on memory usage and we also get
below the 500MB mark of disk usage:

```terminal
df -h --output=source,used,target /
Filesystem      Used Mounted on
/dev/sda1       495M /
```
{: .superuser}

```terminal
free -ht | grep ^Total
Total:  240M  21M 96M
```
{: .superuser}


### Using *dropbear*

*dropbear* is a lightweight *SSH* server designed for small memory environments.
We can install it by running:

```terminal
apt install --no-install-recommends dropbear-run
```
{: .superuser}

We must also enable the service so it starts during boot:

```terminal
sed -i 's,^NO_START=1,NO_START=0,' /etc/default/dropbear
```
{: .superuser}

Now, we can remove *OpenSSH*:

```terminal
apt purge --auto-remove ssh
```
{: .superuser}


### Going really minimal

If we are really short on disk space, we can run *debootstrap* with the
`--variant=minbase` option:

```terminal
sudo debootstrap --arch amd64 --variant=minbase stretch /mnt/debian http://deb.debian.org/debian
```

This option, according to *[debootstrap's manual page][15]*, only installs
the essential packages:

> Currently, the variants supported are minbase, which only includes essential
> packages and apt; buildd, which installs the build-essential packages into
> *TARGET*; and fakechroot, which installs the packages without root privileges.
> The default, with no **\--variant=X** argument, is to create a base Debian
> installation in *TARGET*.
>
> -- <cite markdown="1">[Manpages][15]</cite>

Along with the usual [*debootstrap* configuration](#using-debootstrap), in order
to have a functional environment, we need to install an *[init][13]* system
(such as *[SysV](#removing-systemd)*) and some network tools so we can connect
to the network:

```terminal
apt install --no-install-recommends ifupdown iproute2 isc-dhcp-client netbase
```
{: .superuser}

In the end, we should be able to shave off a few megabytes of disk space by
having less packages than the default *debootstrap* variant, and also use less
memory:

```terminal
df -h --output=source,used,target /
Filesystem      Used Mounted on
/dev/sda1       453M /
```
{: .superuser}

```terminal
free -ht | grep ^Total
Total:  240M  19M 182M
```
{: .superuser}

```terminal
dpkg -l | grep ^ii | wc -l
118
```
{: .superuser}



## References

- [D.3. Installing Debian GNU/Linux from a Unix/Linux System][26]
- [Chapter 5. Network setup][27]
- [ReduceDebian - Debian Wiki][28]
- [ReducingDiskFootprint - Ubuntu Wiki][29]
- [How to remove systemd from a Debian Stretch installation - Without
  Systemd][18]


[1]:  //web.archive.org/web/20190324/en.wikipedia.org/wiki/APT_(Debian)
[2]:  //web.archive.org/web/20190324/www.archlinux.org/
[3]:  //web.archive.org/web/20190324/en.wikipedia.org/wiki/Software_bloat
[4]:  //web.archive.org/web/20190324/en.wikipedia.org/wiki/Chroot
[5]:  //web.archive.org/web/20190324/www.debian.org/
[6]:  //web.archive.org/web/20190324/wiki.debian.org/Debootstrap
[7]:  //web.archive.org/web/20190324/en.wikipedia.org/wiki/dpkg
[8]:  //web.archive.org/web/20190324/en.wikipedia.org/wiki/ext4
[9]:  //web.archive.org/web/20190324/en.wikipedia.org/wiki/Fdisk
[10]: //web.archive.org/web/20190324/distrowatch.com/search.php?basedon=Debian#simple
[11]: //web.archive.org/web/20190324/www.gentoo.org/
[12]: //web.archive.org/web/20190324/en.wikipedia.org/wiki/GParted
[13]: //web.archive.org/web/20190324/en.wikipedia.org/wiki/Init
[14]: //web.archive.org/web/20190324/manpages.debian.org/stretch/ifupdown2/interfaces.5.en.html
[15]: //web.archive.org/web/20190324/manpages.debian.org/stretch/debootstrap/debootstrap.8.en.html
[16]: //web.archive.org/web/20190324/en.wikipedia.org/wiki/Minimalism_(computing)
[17]: //web.archive.org/web/20190324/www.debian.org/distrib/netinst#netboot
[18]: //web.archive.org/web/20190324/without-systemd.org/wiki/index.php/How_to_remove_systemd_from_a_Debian_Stretch_installation
[19]: //web.archive.org/web/20190324/www.openbsd.org/
[20]: //web.archive.org/web/20190324/en.wikipedia.org/wiki/QEMU
[21]: //web.archive.org/web/20190324/www.debian.org/security/
[22]: //web.archive.org/web/20190324/en.wikipedia.org/wiki/Secure_Shell
[23]: //web.archive.org/web/20190324/en.wikipedia.org/wiki/SysV
[24]: //web.archive.org/web/20190324/wiki.debian.org/StableUpdates
[25]: //web.archive.org/web/20190324/voidlinux.org/
[26]: //web.archive.org/web/20190324/www.debian.org/releases/stretch/i386/apds03.html
[27]: //web.archive.org/web/20190324/www.debian.org/doc/manuals/debian-reference/ch05
[28]: //web.archive.org/web/20190324/wiki.debian.org/ReduceDebian
[29]: //web.archive.org/web/20190324/wiki.ubuntu.com/ReducingDiskFootprint
