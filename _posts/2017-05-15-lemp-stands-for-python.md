---
layout:       post
title:        LEMP stands for Python
description:  Using Python instead of PHP in LEMP
lang:         en
ref:          lemp-python
categories:   blog
short:        TKuU
tags:         [windows, linux, unix, vagrant, ansible, nginx, web, server, php, lamp, lemp, python]
---

{% include toc.md %}

In a [previous post]({% post_url 2017-05-08-lemp-with-vagrant-and-ansible %}) we
learned how to setup a [LEMP][9] guest machine with [Vagrant][26] and
[Ansible][1] to test [PHP][12] applications.  However, in this day and age,
there are other programming languages that can be used [for web development][3]
other than PHP.

[Python][17] is a very popular beginner-friendly programming language that will
allow us to create web applications with ease due to its multiple third party
modules and packages.

Unlike PHP, Python was not born as a server-side scripting language designed for
web development and it's not as intertwined with web servers (such as
[Apache][2]). For this reason, it requires some kind of module or gateway to
work.

For this post, we will be installing [uWSGI][25], which is a [Web Server Gateway
Interface][29], inside our Vagrant guest machine and connect [Nginx][10] to
it.

<!--more-->

## Creating a test application

We are going to create a very simple [test application][21] using
[Flask][6].

> Flask is a micro web framework written in Python and based on the Werkzeug
> toolkit and Jinja2 template engine.
>
> -- <cite markdown="1">[Wikipedia][5]</cite>

A [web framework][28] is a great tool that makes developing Python web
applications much easier.

We will save the configuration files for our application in the subdirectory
`vagrant/www/test`:

```
vagrant/www/test
├── requirements.txt
├── test.ini
└── test.py
```


### Requirements file

> A virtual environment is a self-contained directory tree that contains a
> Python installation for a particular version of Python, plus a number of
> additional packages.
>
> -- <cite markdown="1">[Python documentation][27]</cite>

Virtual environments give us the possibility to isolate each Python application.
This way, we can use [PIP][14] to install different versions of packages without
conflicting with versions of the same package for other applications.

And how do we tell PIP which packages and which versions to install? Well, by
using a [requirements file][18] for each application.

Since our test application only needs Flask, we will add it to
`requirements.txt`:

```python
Flask>=0.12
```


### uWSGI's configuration file

To tell uWSGI how to launch our application, we will set some parameters in the
file `test.ini` that will be loaded by uWSGI on boot:

```ini
[uwsgi]
plugins = python3
socket = /tmp/test.sock
venv = /opt/virtualenvs/test
chdir = /vagrant/www/test
wsgi-file = test.py
callable = app
```

Here we specify the version of Python to use, the [socket file][19], the
path of the virtual environment, the directory for our application's files, the
file that contains the application, and what object to call.


### Python application

The web application per se will be in `test.py`:

```python
#!/usr/bin/env python3

from flask import Flask

app = Flask(__name__)


@app.route('/test')
def test():
    return '<p style="background: aliceblue;">Hello Wold</p>\n'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
```

This just will send the text *Hello World* (on a blue background) to the web
browser when we connect to the server.


## Configuration of Ansible

Now, we have to modify our previous [configuration for Ansible]({% post_url
2017-05-08-lemp-with-vagrant-and-ansible %}#configuration-of-ansible) so it
installs all the necessary packages and loads the appropriate configuration
files.


### Global variables

First of all, since we will be using some variables that are common to more than
one role, let's create a file to store these [global variables][7]. In
the subdirectory `vagrant/cfg/group_vars` we create a file called `all.yml` for
all roles:

```yml
---
base_dir:   '/vagrant/www'
venv_dir:   '/opt/virtualenvs'
```

We specify the directory that Nginx will use as root and the directory where
Python's virtual environments will be created.

In this same file, we will include the name/directory of our applications:

```yml
apps:
  - name: test
```


### Python role

Now, let's add a new role to our configuration so all the appropriate Python
dependencies are met. We will save the files in the subdirectory
`vagrant/cfg/roles/python`:

```
vagrant/cfg/roles/python
├── handlers
│   └── main.yml
└── tasks
    └── main.yml
```

Tasks are saved in `tasks/main.yml`:

{% raw %}
```yaml
---
- name: Install Python
  package: name={{ item }} state=present
  with_items:
    - python3-pip
    - python3-venv
    - uwsgi
    - uwsgi-plugin-python3
  notify:
    - start uwsgi

- name: Install PIP packages
  pip:
    requirements: '{{ base_dir }}/{{ item.name }}/requirements.txt'
    virtualenv: '{{ venv_dir }}/{{ item.name }}'
    virtualenv_command: pyvenv
  with_items:
    - '{{ apps }}'

- name: Link uWSGI file
  file:
    src: '{{ base_dir }}/{{ item.name }}/{{ item.name }}.ini'
    dest: '/etc/uwsgi/apps-enabled/{{ item.name }}.ini'
    force: yes
    state: link
  with_items:
    - '{{ apps }}'
  notify:
    - restart uwsgi
```
{% endraw %}

The steps are:

1. Using the [package module][11], we install the Python 3 packages for PIP and
*venv* along with uWSGI and its Python 3 plugin.
2. Then, using the [pip module][13], we read the file `requirements.txt` for
   each application and install its packages in a virtual environment.
3. We then enable the application by adding a [symbolic link][20] to the
application's [INI file][8] in the directory `/etc/uwsgi/apps-enabled` using the
[file module][4].

And let's not forget the handlers to enable and restart the service when needed.
Add these to the file `handlers/main.yml`:

```yaml
---
- name: start uwsgi
  service: name=uwsgi enabled=yes state=started

- name: restart uwsgi
  service: name=uwsgi state=restarted
```

Finally, we modify `vagrant/cfg/site.yml` to add this new role:

```yaml
---
- name: Configure LEMP server
  hosts: lemp
  roles:
    - mariadb
    - php
    - python
    - nginx
```


### Nginx role

We also need to modify the template for our [Nginx role]({% post_url
2017-05-08-lemp-with-vagrant-and-ansible %}#nginx-role) in
`vagrant/cfg/roles/nginx` so it loads our applications.

For this, we edit `templates/default` and add the following inside the *server*
directive:

{% raw %}
```jinja
{% for item in apps %}
  location /{{ item.name }} {
    include uwsgi_params;
    uwsgi_pass unix:/tmp/{{ item.name }}.sock;
  }
{% endfor %}
```
{% endraw %}

This will loop through each of our applications and configure Nginx to use uWSGI
for its corresponding subpath.


## Running the test application

We can now start the guest machine with Vagrant using the command `vagrant up`.
After the machine has finished booting, we will be able to open our test
application by pointing our web browser to <http://172.28.128.10/test>:

{% include postImage.html file="test-app.png" alt="Test application" width=1024
height=200 %}

## Conclusion

Using Python as the backend for our web applications is not as straightforward
as throwing some PHP code in an `index.php` file but, with some tweaking, we can
have a working application and have access to the power of [Python
packages][15].


## Further reading

- [Comparison of web frameworks : Python - Wikipedia][16]
- [Nginx support — uWSGI documentation][24]
- [How to Deploy Python WSGI Applications Using uWSGI Web Server with Nginx |
DigitalOcean][22]
- [How To Set Up uWSGI and Nginx to Serve Python Apps on Ubuntu 14.04 |
DigitalOcean][23]


[1]:  //web.archive.org/web/20170515/www.ansible.com/
[2]:  //web.archive.org/web/20170515/httpd.apache.org/
[3]:  //web.archive.org/web/20170515/en.wikipedia.org/wiki/Server-side_scripting#Languages
[4]:  //web.archive.org/web/20170515/docs.ansible.com/ansible/file_module.html
[5]:  //web.archive.org/web/20170515/en.wikipedia.org/wiki/Flask_(web_framework)
[6]:  //web.archive.org/web/20170515/flask.pocoo.org/
[7]:  //web.archive.org/web/20170515/docs.ansible.com/ansible/intro_inventory.html#default-groups
[8]:  //web.archive.org/web/20170515/uwsgi-docs.readthedocs.io/en/latest/Configuration.html#ini-files
[9]:  //web.archive.org/web/20170515/en.wikipedia.org/wiki/LAMP_(software_bundle)#Variants
[10]: //web.archive.org/web/20170515/nginx.org/
[11]: //web.archive.org/web/20170515/docs.ansible.com/ansible/package_module.html
[12]: //web.archive.org/web/20170515/en.wikipedia.org/wiki/PHP
[13]: //web.archive.org/web/20170515/docs.ansible.com/ansible/pip_module.html
[14]: //web.archive.org/web/20170515/pip.pypa.io/
[15]: //web.archive.org/web/20170515/pypi.python.org/pypi
[16]: //web.archive.org/web/20170515/en.wikipedia.org/wiki/Comparison_of_web_frameworks#Python
[17]: //web.archive.org/web/20170515/en.wikipedia.org/wiki/Python_(programming_language)
[18]: //web.archive.org/web/20170515/pip.pypa.io/en/stable/user_guide/#requirements-files
[19]: //web.archive.org/web/20170515/en.wikipedia.org/wiki/Socket_file
[20]: //web.archive.org/web/20170515/en.wikipedia.org/wiki/Symlink
[21]: //web.archive.org/web/20170515/uwsgi-docs.readthedocs.io/en/latest/WSGIquickstart.html#deploying-flask
[22]: //web.archive.org/web/20170515/www.digitalocean.com/community/tutorials/how-to-deploy-python-wsgi-applications-using-uwsgi-web-server-with-nginx
[23]: //web.archive.org/web/20170515/www.digitalocean.com/community/tutorials/how-to-set-up-uwsgi-and-nginx-to-serve-python-apps-on-ubuntu-14-04
[24]: //web.archive.org/web/20170515/uwsgi-docs.readthedocs.io/en/latest/Nginx.html
[25]: //web.archive.org/web/20170515/uwsgi-docs.readthedocs.io/
[26]: //web.archive.org/web/20170515/www.vagrantup.com/
[27]: //web.archive.org/web/20170515/docs.python.org/tutorial/venv.html
[28]: //web.archive.org/web/20170515/en.wikipedia.org/wiki/Web_framework
[29]: //web.archive.org/web/20170515/en.wikipedia.org/wiki/WSGI
