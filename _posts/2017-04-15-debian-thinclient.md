---
layout:       post
title:        Thin client with Debian and xrdp
description:  Install Debian 9 with Remmina to connect to xrdp
lang:         en
ref:          debian-thinclient
categories:   blog
short:        kOag
tags:         [linux, debian, thinclient, remote desktop]
---

{% include toc.md %}

If you're anything like me, you'll probably have at least one old PC or laptop
collecting dust somewhere in your house. There are many ways to [give new life
to these devices][16], but one very simple option is to use them as [thin
clients][13] to remotely access a more powerful and modern machine.

<!--more-->

## Server

Let's configure our PC so it will act as the server for us to connect to.
Assuming we already have [Debian][2] installed, we will just need to install the
server software.


### xrdp

For this purpose, we will use [xrdp][20] which is an open source server for the
[RDP][9] protocol. To install it just run:

```terminal
sudo apt install xrdp
```


#### Securing the connection

> Standard RDP Security, which is not safe from man-in-the-middle attack, is
> used. The encryption level of Standard RDP Security is controlled by
> **crypt_level**.
>
> -- <cite markdown="1">[Manpages][21]</cite>

We definitely should do something about this so we will use [TLS][15] as the
security layer.

The necessary certificates were generated automatically during the installation
of the *ssl-cert* package but we need to add user *xrdp* to this group so it can
read the private key:

```terminal
sudo adduser xrdp ssl-cert
```

Now let's edit file `/etc/xrdp/xrdp.ini` and change these:

```ini
security_layer=negotiate
certificate=
key_file=
```

To these:

```ini
security_layer=tls
certificate=/etc/xrdp/cert.pem
key_file=/etc/xrdp/key.pem
```

And restart the service:

```terminal
sudo service xrdp restart
```

With this, our PC is ready to be accessed remotely by the thin client.


## Client

We can now go ahead and set up the box that will act as thin client.

For this we just need a bare bones [Debian 9 (*stretch*)][1] install with
only a few extra packages. Therefore, we will make sure nothing is selected
during the *Software selection* step of the installation process:

{% include postImage.html file="software-selection.png" alt="Debian software
selection" width=800 height=600 %}

Afterwards, once we have booted into Debian, we can install a [display
manager][3] and a [window manager][18] so we can have a simple graphical
environment. We will use [LightDM][5] and [Openbox][8] respectively, and
[tint2][14] as a lightweight taskbar:

```terminal
sudo apt install lightdm openbox tint2 xterm
```

After a reboot, we will be presented with the login screen:

{% include postImage.html file="debian-login.png" alt="Debian login screen"
width=1024 height=768 %}

Let's configure Openbox so it launches tint2 after we login. We need to copy the
default configuration files for Openbox to our home directory so we can modify
them:

```terminal
mkdir -p ~/.config/openbox && cp /etc/xdg/openbox/* ~/.config/openbox
```

Then we will edit the file `~/.config/openbox/autostart` to add, at the end, the
following lines:

```shell
# Launch taskbar
tint2 &
```


### Remmina

To access our server remotely, we will use [Remmina][10] as it supports several
protocols ([RDP][9], [VNC][17], [SSH][12], [NX][7], [XDMCP][19], etc.):

```terminal
sudo apt install remmina
```

Once the installation finishes, we will again edit Openbox's `autostart` file to
launch Remmina:

```shell
# Start Remmina
remmina &
```

After we login, we can create a new connection in Remmina by pressing
**Ctrl+N**. We just have to enter a name for the connection and the address or
hostname of our server:

{% include postImage.html file="new-connection.png" alt="Remmina new connection"
width=1024 height=768 %}

After we click on *Connect*, the connection will be saved and we will be asked
to accept the server's TLS certificate:

{% include postImage.html file="accept-cert.png" alt="Certificate details"
width=1024 height=768 %}

If we accept it, we will get to the login screen for our remote server:

{% include postImage.html file="remote-login1.png" alt="Remote login"
width=1024 height=768 %}

Here we just need to enter the username and password for our remote server and
we will have access to the desktop. By pressing **R_Ctrl+F** we can make it
fullscreen for a seamless experience:

{% include postImage.html file="remote-desktop.png" alt="Remote desktop"
width=1024 height=768 %}


#### Show remote login screen after local boot

We can configure our Debian thin client to automatically login and make Remmina
launch the connection to our remote server so we are presented with the remote
login screen.

First we will configure LightDM to automatically login with our local user. For
this, we need to edit the file `/etc/lightdm/lightdm.conf` as root and configure
our username in the *Seat configuration* section:

```ini
[Seat:*]
autologin-user=agus
```

Now, we need to find out the filename for our connection as it was saved by
Remmina. Connections are either in `$HOME/.remmina`, for older versions, or in
`$XDG_DATA_HOME/remmina` for newer ones.

```
~/.remmina
├── 1492192074855.remmina
└── remmina.pref
```

Then we just have to modify the last line of Openbox's `autostart` file
accordingly:

```shell
# Start Remmina
remmina -c ~/.remmina/1492192074855.remmina &
```

If we reboot now, we will connect directly to the remote machine and be
presented with its login screen:

{% include postImage.html file="remote-login2.png" alt="Remote login"
width=1024 height=768 %}


#### Login to remote server after local login

We can configure Remmina to save the remote login credentials and log us into
the server automatically.

For this, we need to save the username and password on the connection profile:

{% include postImage.html file="save-credentials.png" alt="Save credentials"
width=1024 height=768 %}

However, for [extra security][11], we should install GNOME plugin for
Remmina so the password is stored in [GNOME keyring][4]:

```terminal
sudo apt install remmina-plugin-gnome seahorse
```

We will need to logout and log back in so the keyring is generated transparently
using our local password.

Now, if we modify the connection and add the credentials, the password will be
stored in the keyring for safekeeping:

{% include postImage.html file="gnome-keyring.png" alt="GNOME keyring"
width=1024 height=768 %}

Since our local password is needed to unlock the keyring to retrieve the remote
password, we will have to revert the changes in `/etc/lightdm/lightdm.conf`:

```ini
[Seat:*]
#autologin-user=agus
```

Otherwise, it will ask us to unlock the keyring before Remmina can connect to
our remote server.


### Tidying up

Since this box won't be doing much work other than running Remmina to
connect to our server, we can remove some unneeded packages. Things like job
scheduling and message logging are pointless:

```terminal
sudo apt purge --auto-remove anacron cron rsyslog
```

As an added note, if we plan to connect to a wireless network, we might want to
install [NetworkManager][6] and its applet to make it easier for us:

```terminal
sudo apt install network-manager-gnome
```

However, since NetworkManager uses GNOME keyring, we won't be able to
[automatically login locally](#show-remote-login-screen-after-local-boot) in a
seamless way.


## Conclusion

As we have seen, using an old PC or laptop as a thin client is a great way to
give new life to these devices.

The hardware requirements are very low since it will be mostly using the
network. You can see the resource usage in a system with only 128 MB of RAM:

{% include postImage.html file="resource-usage.png" alt="Resource usage"
width=1024 height=768 %}


## Further reading

- [Problem found with Debian systems running systemd · Issue #190 ·
neutrinolabs/xrdp · GitHub][22]
- [TLS security layer · neutrinolabs/xrdp Wiki · GitHub][23]
- [Remmina Usage FAQ · FreeRDP/Remmina Wiki · GitHub][24]
- [Audio Output Virtual Channel support in xrdp · neutrinolabs/xrdp Wiki ·
GitHub][25]
- [TransparentEncryptionForHomeFolder - Debian Wiki][26]
- [Linux Terminal Server Project - Welcome to LTSP.org][27]
- [ThinStation by Donald A. Cupp Jr.][28]


[1]:  //web.archive.org/web/20170415/www.debian.org/releases/stretch/
[2]:  //web.archive.org/web/20170415/www.debian.org/
[3]:  //web.archive.org/web/20170415/en.wikipedia.org/wiki/Display_manager
[4]:  //web.archive.org/web/20170415/wiki.gnome.org/Projects/GnomeKeyring
[5]:  //web.archive.org/web/20170415/freedesktop.org/wiki/Software/LightDM/
[6]:  //web.archive.org/web/20170415/wiki.gnome.org/Projects/NetworkManager
[7]:  //web.archive.org/web/20170415/en.wikipedia.org/wiki/NX_technology
[8]:  //web.archive.org/web/20170415/openbox.org/
[9]:  //web.archive.org/web/20170415/en.wikipedia.org/wiki/Remote_Desktop_Protocol
[10]: //web.archive.org/web/20170415/www.remmina.org/
[11]: //web.archive.org/web/20170415/askubuntu.com/q/290824
[12]: //web.archive.org/web/20170415/en.wikipedia.org/wiki/Secure_Shell
[13]: //web.archive.org/web/20170415/en.wikipedia.org/wiki/Thin_client
[14]: //web.archive.org/web/20170415/gitlab.com/o9000/tint2
[15]: //web.archive.org/web/20170415/en.wikipedia.org/wiki/Transport_Layer_Security
[16]: //duckduckgo.com/?q=use+old+pc
[17]: //web.archive.org/web/20170415/en.wikipedia.org/wiki/Virtual_Network_Computing
[18]: //web.archive.org/web/20170415/en.wikipedia.org/wiki/Window_manager
[19]: //web.archive.org/web/20170415/en.wikipedia.org/wiki/XDMCP
[20]: //web.archive.org/web/20170415/www.xrdp.org/
[21]: //web.archive.org/web/20170415/manpages.debian.org/stretch/xrdp/xrdp.ini.5.en.html
[22]: //web.archive.org/web/20170415/github.com/neutrinolabs/xrdp/issues/190
[23]: //web.archive.org/web/20170415/github.com/neutrinolabs/xrdp/wiki/TLS-security-layer
[24]: //web.archive.org/web/20170415/github.com/FreeRDP/Remmina/wiki/Remmina-Usage-FAQ
[25]: //web.archive.org/web/20170415/github.com/neutrinolabs/xrdp/wiki/Audio-Output-Virtual-Channel-support-in-xrdp
[26]: //web.archive.org/web/20170415/wiki.debian.org/TransparentEncryptionForHomeFolder
[27]: //web.archive.org/web/20170415/www.ltsp.org/
[28]: //web.archive.org/web/20170415/www.thinstation.org/
