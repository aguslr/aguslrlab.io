---
layout:       post
title:        LEMP significa Python
description:  Usando Python en lugar de PHP en LEMP
lang:         es
ref:          lemp-python
categories:   blog
short:        odEs
tags:         [windows, linux, unix, vagrant, ansible, nginx, web, server, php, lamp, lemp, python]
---

{% include toc.md %}

En la [entrada anterior]({% post_url 2017-05-08-lemp-con-vagrant-y-ansible %})
aprendimos cómo configurar una máquina huésped [LEMP][8] con [Vagrant][25] y
[Ansible][1] para probar aplicaciones [PHP][13]. Sin embargo, en la actualidad,
se pueden usar otros lenguajes de programación [para el desarrollo
web][3] aparte de PHP.

[Python][17] es un popular lenguaje de programación, de fácil aprendizaje, que
nos permitirá crear con facilidad aplicaciones web gracias a sus múltiples
módulos y paquetes de terceros.

A diferencia de PHP, Python no nació con el objetivo de ser un lenguaje de
servidor diseñado para el desarrollo web y no está tan integrado con servidores
web (tales como [Apache][2]). Es por esto que deberemos usar algún tipo de
módulo o pasarela para que funcione.

Para esta entrada, instalaremos [uWSGI][24], una [interfaz de pasarela de
servidor web (WSGI)][28], dentro de la máquina huésped de Vagrant y conectaremos
[Nginx][12] a ésta.

<!--more-->

## Creando una aplicación de prueba

Vamos a crear una simple [aplicación de prueba][20] usando [Flask][6].

> Flask es un framework minimalista escrito en Python que te permite crear
> aplicaciones web rápidamente y con un mínimo número de líneas de código.
>
> -- <cite markdown="1">[Wikipedia][5]</cite>

Un [framework para aplicaciones web][27] es una gran herramienta que
facilita mucho el desarrollo de aplicaciones web en Python.

Vamos a guardar los archivos de configuración de nuestra aplicación en el
subdirectorio `vagrant/www/test`:

```
vagrant/www/test
├── requirements.txt
├── test.ini
└── test.py
```


### Archivo *requirements*

> Un entorno virtual es un árbol de directorios independiente que contiene una
> instalación de Python para una versión específica de Python, además de un
> número adicional de paquetes
>
> -- <cite markdown="1">[Documentación de Python][26]</cite>

Los entornos virtuales nos dan la posibilidad de aislar cada una de nuestras
aplicaciones Python. De esta forma, podemos usar [PIP][14] para instalar
distintas versiones de paquetes sin que haya conflictos con las versiones del
mismo paquete para otra aplicación.

¿Y cómo le decimos a PIP qué paquetes y qué versiones debe instalar? Pues usando
un [archivo *requirements*][18] para cada aplicación.

Puesto que nuestra aplicación de prueba sólo necesita Flask, lo añadiremos al
archivo `requirements.txt`:

```python
Flask>=0.12
```


### Archivo de configuración de uWSGI

Para indicar a uWSGI cómo lanzar nuestra aplicación, configuraremos algunos
parámetros en el archivo `test.ini` que será cargado por uWSGI durante el
arranque:

```ini
[uwsgi]
plugins = python3
socket = /tmp/test.sock
venv = /opt/virtualenvs/test
chdir = /vagrant/www/test
wsgi-file = test.py
callable = app
```

Aquí especificamos la versión de Python a usar, el [archivo *socket*][19],
la ruta del entorno virtual, el directorio de los archivos de nuestra
aplicación, el archivo que contiene la aplicación y qué objeto se debe llamar.


### Aplicación Python

La aplicación web *per se* estará en `test.py`:

```python
#!/usr/bin/env python3

from flask import Flask

app = Flask(__name__)


@app.route('/test')
def test():
    return '<p style="background: aliceblue;">Hello Wold</p>\n'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
```

Esto simplemente manda el texto *Hello World* (sobre un fondo azul) al navegador
web cuando nos conectamos al servidor.


## Configuración de Ansible

Ahora deberemos modificar nuestra [configuración de Ansible]({% post_url
2017-05-08-lemp-con-vagrant-y-ansible %}#configuracin-de-ansible) de forma que
instale todos los paquetes necesarios y cargue los archivos de configuración
apropiados.


### Variables globales

Antes de nada, puesto que usaremos algunas variables que son comunes a más de un
rol, vamos a crear un archivo para almacenar estas [variables
globales][7]. En el subdirectorio `vagrant/cfg/group_vars` creamos un
archivo llamado `all.yml` para todos los roles:

```yml
---
base_dir:   '/vagrant/www'
venv_dir:   '/opt/virtualenvs'
```

Indicamos el directorio que Nginx usará como raíz y el directorio en el que se
crearán los entornos virtuales de Python.

En este mismo archivo, incluiremos el nombre/directorio de nuestras
aplicaciones:

```yml
apps:
  - name: test
```


### Rol Python

Ahora vamos a añadir un nuevo rol a nuestra configuración de forma que se
cumplan todas las dependencias de Python necesarias. Guardaremos los archivos en
el subdirectorio `vagrant/cfg/roles/python`:

```
vagrant/cfg/roles/python
├── handlers
│   └── main.yml
└── tasks
    └── main.yml
```

Las tareas se guardan en `tasks/main.yml`:

{% raw %}
```yaml
---
- name: Instala Python
  package: name={{ item }} state=present
  with_items:
    - python3-pip
    - python3-venv
    - uwsgi
    - uwsgi-plugin-python3
  notify:
    - arranca uwsgi

- name: Instala paquetes PIP
  pip:
    requirements: '{{ base_dir }}/{{ item.name }}/requirements.txt'
    virtualenv: '{{ venv_dir }}/{{ item.name }}'
    virtualenv_command: pyvenv
  with_items:
    - '{{ apps }}'

- name: Enlace archivo uWSGI
  file:
    src: '{{ base_dir }}/{{ item.name }}/{{ item.name }}.ini'
    dest: '/etc/uwsgi/apps-enabled/{{ item.name }}.ini'
    force: yes
    state: link
  with_items:
    - '{{ apps }}'
  notify:
    - reinicia uwsgi
```
{% endraw %}

Los pasos son:

1. Usando el [módulo package][10], instalamos los paquetes PIP y *venv* de
   Python 3, así como uWSGI y su plugin de Python 3.  2. Luego, usando el
[módulo pip][11], leemos el archivo `requirements.txt` para cada aplicación e
instalamos sus paquetes en un entorno virtual.
3. Activamos entonces la aplicación creando un [enlace simbólico][4] al [archivo
   INI][29] de la aplicación en el directorio `/etc/uwsgi/apps-enabled` usando
el [módulo file][9].

Y no nos olvidemos de los *handlers* para activar y reiniciar el servicio cuando
sea necesario. Añadimos esto al archivo `handlers/main.yml`:

```yml
---
- name: arranca uwsgi
  service: name=uwsgi enabled=yes state=started

- name: reinicia uwsgi
  service: name=uwsgi state=restarted
```

Por último, modificamos `vagrant/cfg/site.yml` para añadir este nuevo rol:

```yaml
---
- name: Configura servidor LEMP
  hosts: lemp
  roles:
    - mariadb
    - php
    - python
    - nginx
```


### Rol Nginx

También tenemos que modificar la plantilla para nuestro [rol Nginx]({% post_url
2017-05-08-lemp-con-vagrant-y-ansible %}#rol-nginx) en
`vagrant/cfg/roles/nginx` de forma que cargue nuestras aplicaciones.

Para ello, editamos `templates/default` y añadimos lo siguiente dentro de la
directiva *server*:

{% raw %}
```jinja
{% for item in apps %}
  location /{{ item.name }} {
    include uwsgi_params;
    uwsgi_pass unix:/tmp/{{ item.name }}.sock;
  }
{% endfor %}
```
{% endraw %}

Esto configurará Nginx para usar cada una de nuestras aplicaciones para su
correspondiente subruta.


## Ejecutando la aplicación de prueba

Podemos ahora iniciar nuestra máquina huésped con Vagrant usando el comando
`vagrant up`. Tras el arranque, podremos abrir nuestra aplicación de prueba
dirigiendo nuestro navegador web a <http://172.28.128.10/test>:

{% include postImage.html file="test-app.png" alt="Aplicación de prueba"
width=1024 height=200 %}

## Conclusión

Usar Python como *backend* para nuestras aplicaciones web no es tan sencillo
como poner unas cuantas líneas de código PHP en el archivo `index.php` pero, con
unos pocos cambios, podemos crear una aplicación funcional y tener acceso al
potencial de los [paquetes de Python][15].


## Leer más

- [Comparison of web frameworks : Python - Wikipedia][16]
- [Nginx support — uWSGI documentation][23]
- [How to Deploy Python WSGI Applications Using uWSGI Web Server with Nginx |
DigitalOcean][21]
- [How To Set Up uWSGI and Nginx to Serve Python Apps on Ubuntu 14.04 |
DigitalOcean][22]


[1]:  //web.archive.org/web/20170515/www.ansible.com/
[2]:  //web.archive.org/web/20170515/httpd.apache.org/
[3]:  //web.archive.org/web/20170515/es.wikipedia.org/wiki/Script_del_lado_del_servidor#Lenguajes
[4]:  //web.archive.org/web/20170515/es.wikipedia.org/wiki/Enlace_simb%C3%B3lico
[5]:  //web.archive.org/web/20170515/es.wikipedia.org/wiki/Flask
[6]:  //web.archive.org/web/20170515/flask.pocoo.org/
[7]:  //web.archive.org/web/20170515/docs.ansible.com/ansible/intro_inventory.html#default-groups
[8]:  //web.archive.org/web/20170515/es.wikipedia.org/wiki/LAMP#Variantes_y_Alternativas
[9]:  //web.archive.org/web/20170515/docs.ansible.com/ansible/file_module.html
[10]: //web.archive.org/web/20170515/docs.ansible.com/ansible/package_module.html
[11]: //web.archive.org/web/20170515/docs.ansible.com/ansible/pip_module.html
[12]: //web.archive.org/web/20170515/nginx.org/
[13]: //web.archive.org/web/20170515/es.wikipedia.org/wiki/PHP
[14]: //web.archive.org/web/20170515/pip.pypa.io/
[15]: //web.archive.org/web/20170515/pypi.python.org/pypi
[16]: //web.archive.org/web/20170515/en.wikipedia.org/wiki/Comparison_of_web_frameworks#Python
[17]: //web.archive.org/web/20170515/es.wikipedia.org/wiki/Python
[18]: //web.archive.org/web/20170515/pip.pypa.io/en/stable/user_guide/#requirements-files
[19]: //web.archive.org/web/20170515/es.wikipedia.org/wiki/Socket_Unix
[20]: //web.archive.org/web/20170515/uwsgi-docs.readthedocs.io/en/latest/WSGIquickstart.html#deploying-flask
[21]: //web.archive.org/web/20170515/www.digitalocean.com/community/tutorials/how-to-deploy-python-wsgi-applications-using-uwsgi-web-server-with-nginx
[22]: //web.archive.org/web/20170515/www.digitalocean.com/community/tutorials/how-to-set-up-uwsgi-and-nginx-to-serve-python-apps-on-ubuntu-14-04
[23]: //web.archive.org/web/20170515/uwsgi-docs.readthedocs.io/en/latest/Nginx.html
[24]: //web.archive.org/web/20170515/uwsgi-docs.readthedocs.io/
[25]: //web.archive.org/web/20170515/www.vagrantup.com/
[26]: //web.archive.org/web/20170515/docs.python.org/tutorial/venv.html
[27]: //web.archive.org/web/20170515/es.wikipedia.org/wiki/Framework_para_aplicaciones_web
[28]: //web.archive.org/web/20170515/en.wikipedia.org/wiki/WSGI
[29]: //web.archive.org/web/20170515/uwsgi-docs.readthedocs.io/en/latest/Configuration.html#ini-files
