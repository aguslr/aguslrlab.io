---
layout:       post
title:        Installing Mandrake 9.1 in QEMU
description:  Installing Mandrake 9.1 (2003) in a QEMU virtual machine
lang:         en
ref:          mandrake-91
categories:   blog
short:        dLlI
tags:         [linux, mandrake, qemu, retro]
---

{% include toc.md %}

Inspired by an [article about test driving old Linux distros][31], I've
decided to try installing some of the distributions I have used since my first
contact with Linux 17 years ago.

Although I've been using [Debian][7] since I installed [Debian 3.1][6] back in
2004, the distro that previously allowed me to use Linux exclusively was
[Mandrake 9.1][17].

<!--more-->

## Downloading the ISOs

Unfortunately, Mandrake (later renamed to [Mandriva][18]), has been defunct
since 2011 but, thanks to the wonderful [Internet Archive][14], we can still get
[the ISO files for Mandrake 9.1][29]:

```terminal
wget https://archive.org/download/Mandrake91/Mandrake91-cd{1-inst,2-ext,3-i18n}.i586.iso
```


## Installation

Once we have the ISO files, we can start the installation using [QEMU][22].
Therefore, we have to install the necessary package:

```terminal
sudo apt install qemu
```
First of all, let's create a disk image:

```terminal
qemu-img create -f qcow2 mandrake91.qcow2 10G
```

Now, to run the installation, we need to point QEMU to the first ISO and to the
disk image we just created:

```terminal
qemu-system-i386 -m 128 \
  -cdrom Mandrake91-cd1-inst.i586.iso \
  -hda mandrake91.qcow2 -usb \
  -vga cirrus -soundhw ac97 \
  -nic user,model=rtl8139 \
  -boot order=dc
```

Surprisingly, the installation felt as easy now as it did back then, and the
default settings can be left unchanged.

At some point, and depending on the packages we have selected for installation,
we may be asked to [change the installation CD][30]:

{% include postImage.html file="install-step07-2.png" alt="Change your Cd-Rom"
width=800 height=600 %}

For this, we need to press **Ctrl+Alt+2** to access QEMU's monitor interface,
and enter this at the prompt for the second CD:

```
change ide1-cd0 Mandrake91-cd2-ext.i586.iso
```

Or this for the third:

```
change ide1-cd0 Mandrake91-cd3-i18n.i586.iso
```

After we press **Ctrl+Alt+1** to go back to the installation, we can then click
*OK* to continue the process.

Once all packages are installed, we'll have to provide a password for *root* and
create a user.

Then, a summary of the installation process will be shown. We will probably want
to launch the configuration for the *Graphical Interface* by clicking on the
*Configure* button:

{% include postImage.html file="install-step11.png" alt="Summary" width=800
height=600 %}

Thankfully, the auto-detected defaults are fine and we won't need to change
anything. Although we might want to choose a higher resolution and color depth:

{% include postImage.html file="install-step15.png" alt="Choose resolution"
width=800 height=600 %}

After several other uneventful steps, the installation will finish and we will
be free to reboot the system:

{% include postImage.html file="install-step20.png" alt="Reboot the system"
width=800 height=600 %}


## Booting the system

We can boot into Mandrake by running QEMU again. This time, however, we exclude
the ISO file:

```terminal
qemu-system-i386 -m 128 \
  -hda mandrake91.qcow2 -usb \
  -vga cirrus -soundhw ac97 \
  -nic user,modle=rtl8139
```

After the [boot loader][5] and some other boot messages, we will see the login
screen:

{% include postImage.html file="login-screen.png" alt="Mandrake login screen"
width=1024 height=768 %}

Once we've logged in with our user, Mandrake's *First Time Wizard* will be
launched so we can pick our preferred [desktop environment][4] ([KDE][15],
[GNOME][9] or [IceWM][12]). I've chosen KDE because it's what I used back in the
day:

{% include postImage.html file="welcome.png" alt="Welcome to Mandrake"
width=1024 height=768 %}

If we start a console, we'll get to check what version of the Linux kernel we
are running with the command `uname -a`:

```shell
Linux localhost 2.4.21-0.13mdk #1 Fri Mar 14 15:08:06 EST 2003 i686 unknown unknown GNU/Linux
```


### Settings

To change desktop settings like the background, theme or window decorations, we
simply have to launch *KDE Control Center*:

{% include postImage.html file="kde-control-center.png" alt="KDE Control Center"
width=1024 height=768 %}

In order to change any system setting, however, we will need to start *Mandrake
Control Center*. After entering our *root* password, we'll be able to change
things like the boot options, network settings, users and security settings:

{% include postImage.html file="mandrake-control-center.png" alt="Mandrake
Control Center" width=1024 height=768 %}


### Package manager

From *Mandrake Control Center*, we can also do all the package management we
need using *rpmdrake*. This tool automatically handles dependencies so we won't
have to hunt for the required packages:

{% include postImage.html file="software-installation.png" alt="Software
Packages Installation" width=1024 height=768 %}


### File manager

The file manager is [Konqueror][16] which is still kicking today, even if only
as KDE's default web browser:

{% include postImage.html file="konqueror-about.png" alt="File manager"
width=1024 height=768 %}


### Web browser

Unfortunately, as a web browser, this version of Konqueror isn't able to handle
many modern websites. And neither was [Mozilla][19]'s web browser:

{% include postImage.html file="mozilla-about.png" alt="Mozilla web browser"
width=1024 height=768 %}

Of the websites I tried, these two web browsers were only able to load
[Google][10]:

{% include postImage.html file="konqueror-google.png" alt="Google - Konqueror"
width=1024 height=768 %}

{% include postImage.html file="mozilla-google.png" alt="Google - Mozilla"
width=1024 height=768 %}

They both failed to load [other top ranking websites][32] such as
[YouTube][28], [Facebook][8], [Wikipedia][25], [Yahoo][27], [Reddit][23],
[Twitter][24], [Amazon][1] or [Instagram][13].


### Office tools

I was surprised to find out that [OpenOffice][21]'s suite was already around
back in 2003, with its usual components such as Writer, Calc, Impress, etc.:

{% include postImage.html file="openoffice-about.png" alt="OpenOffice.org
Writer" width=1024 height=768 %}


### Multimedia

When it comes to multimedia playback, it was necessary to install [xine][26],
since neither [MPlayer][20] nor KDE's default viewer were able to play [Big Buck
Bunny][3]'s [AVI file][2]:

{% include postImage.html file="video-player.png" alt="Video player" width=1024
height=768 %}

Still, the playback, both for audio and video, was choppy. Maybe the results
would improve by trying other emulated video and audio devices.


## Conclusion

I was pleasantly surprised by how user friendly this version of Mandrake Linux
was, even compared to modern operating systems.

Common day-to-day user actions, and even most system administration, can be
performed using some kind of [GUI][11], without giving up the power of the [Unix
shell][33] underneath.

Granted, most of the issues that would have been encountered back in the 2000's,
regarding Linux, would be related to hardware compatibility and lack of drivers.
This time around, we luckily avoided all that by using a virtual machine.

Either way, the effort that went into creating such an experience for the user
is notable. Especially by the fact that only outside factors, such as vendor
support, could prevent us from enjoying this distro to its full potential.


[1]:  //web.archive.org/web/20170422/www.amazon.com/
[2]:  //web.archive.org/web/20170422/download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_480p_stereo.avi
[3]:  //web.archive.org/web/20170422/peach.blender.org/
[4]:  //web.archive.org/web/20170422/en.wikipedia.org/wiki/Desktop_environment
[5]:  //web.archive.org/web/20170422/en.wikipedia.org/wiki/Boot_loader
[6]:  //web.archive.org/web/20170422/distrowatch.com/2699
[7]:  //web.archive.org/web/20170422/www.debian.org/
[8]:  //web.archive.org/web/20170422/www.facebook.com/
[9]:  //web.archive.org/web/20170422/en.wikipedia.org/wiki/GNOME
[10]: //web.archive.org/web/20170422/www.google.com/
[11]: //web.archive.org/web/20170422/en.wikipedia.org/wiki/GUI
[12]: //web.archive.org/web/20170422/en.wikipedia.org/wiki/IceWM
[13]: //web.archive.org/web/20170422/www.instagram.com/
[14]: //web.archive.org/web/20170422/en.wikipedia.org/wiki/Internet_Archive
[15]: //web.archive.org/web/20170422/en.wikipedia.org/wiki/KDE
[16]: //web.archive.org/web/20170422/en.wikipedia.org/wiki/Konqueror
[17]: //web.archive.org/web/20170422/distrowatch.com/685
[18]: //web.archive.org/web/20170422/en.wikipedia.org/wiki/Mandriva_Linux
[19]: //web.archive.org/web/20170422/en.wikipedia.org/wiki/Mozilla_Suite
[20]: //web.archive.org/web/20170422/en.wikipedia.org/wiki/MPlayer
[21]: //web.archive.org/web/20170422/en.wikipedia.org/wiki/OpenOffice.org
[22]: //web.archive.org/web/20170422/www.qemu.org
[23]: //web.archive.org/web/20170422/www.reddit.com/
[24]: //web.archive.org/web/20170422/twitter.com/
[25]: //web.archive.org/web/20170422/www.wikipedia.org/
[26]: //web.archive.org/web/20170422/en.wikipedia.org/wiki/Xine
[27]: //web.archive.org/web/20170422/www.yahoo.com/
[28]: //web.archive.org/web/20170422/www.youtube.com/
[29]: //archive.org/details/Mandrake91
[30]: //web.archive.org/web/20170422/www.linux-kvm.org/page/Change_cdrom
[31]: //web.archive.org/web/20170422/opensource.com/article/16/12/yearbook-linux-test-driving-distros
[32]: //web.archive.org/web/20170422/www.alexa.com/topsites
[33]: //web.archive.org/web/20170422/en.wikipedia.org/wiki/Unix_shell
