---
layout:       post
title:        Instalando Mandrake 9.1 en QEMU
description:  Instalando Mandrake 9.1 (2003) en una máquina virtual QEMU
lang:         es
ref:          mandrake-91
categories:   blog
short:        Axze
tags:         [linux, mandrake, qemu, retro]
---

{% include toc.md %}

Inspirado por un [artículo que probaba viejas distribuciones de
Linux][27], he decidido intentar instalar algunas de las distribuciones
que he usado desde mi primer contacto con Linux hace 17 años.

Aunque llevo usando [Debian][8] desde que instalé [Debian 3.1][7] en el 2004, la
distribución que previamente me había permitido usar Linux de forma exclusiva
fue [Mandrake 9.1][19].

<!--more-->

## Descargando las ISOs

Desgraciadamente, Mandrake (más tarde rebautizada como [Mandriva][20]),
desapareció en 2011 pero, gracias al maravilloso [Archivo de Internet][2], aún
podemos obtener [los archivos ISO para Mandrake 9.1][9]:

```terminal
wget https://archive.org/download/Mandrake91/Mandrake91-cd{1-inst,2-ext,3-i18n}.i586.iso
```


## Instalación

Una vez hayamos descargado los archivos ISO, podemos empezar la instalación
usando [QEMU][24]. Por lo tanto, debemos instalar el paquete necesario:

```terminal
sudo apt install qemu
```
Antes de nada, vamos a crear una imagen de disco:

```terminal
qemu-img create -f qcow2 mandrake91.qcow2 10G
```

Ahora, para lanzar la instalación, necesitamos indicar a QEMU la primera ISO y
la imagen de disco que acabamos de crear:

```terminal
qemu-system-i386 -m 128 \
  -cdrom Mandrake91-cd1-inst.i586.iso \
  -hda mandrake91.qcow2 -usb \
  -vga cirrus -soundhw ac97 \
  -nic user,model=rtl8139 \
  -boot order=dc
```

Sorprendentemente, la instalación parece tan sencilla ahora como lo fue en su
tiempo, y los ajustes por defecto se pueden dejar sin cambiar.

En un momento dado, y dependiendo de los paquetes que hayamos seleccionado,
quizás se nos pida [cambiar el CD de instalación][5]:

{% include postImage.html file="install-step07-2.png" alt="Cambia tu CD-ROM"
width=800 height=600 %}

Para ello, necesitamos pulsar **Ctrl+Alt+2** para acceder a la interfaz de
monitorización de QEMU, e introducir lo siguiente para el segundo CD:

```
change ide1-cd0 Mandrake91-cd2-ext.i586.iso
```

O esto para el tercero:

```
change ide1-cd0 Mandrake91-cd3-i18n.i586.iso
```

Tras pulsar **Ctrl+Alt+1** para volver a la instalación, podemos pulsar sobre
*Aceptar* para continuar el proceso.

Una vez que se hayan instalado todos los paquetes, deberemos proporcionar la
contraseña de *root* y crear un usuario.

Entonces, se mostrará un resumen del proceso de instalación. Probablemente
nos interese lanzar la configuración para la *Interfaz gráfica* pulsando sobre
el botón de *Configurar*:

{% include postImage.html file="install-step11.png" alt="Resumen" width=800
height=600 %}

Afortunadamente, los parámetros detectados por defecto están bien y no
necesitaremos cambiar nada. Aunque quizás deberíamos elegir una mayor resolución
y profundidad de color:

{% include postImage.html file="install-step15.png" alt="Elige una resolución"
width=800 height=600 %}

Tras varios pasos sin mayor complejidad, la instalación habrá finalizado y
podremos reiniciar el sistema:

{% include postImage.html file="install-step20.png" alt="Reinicia el sistema"
width=800 height=600 %}


## Arrancando el sistema

Podemos arrancar Mandrake lanzando QEMU de nuevo. Esta vez, sin embargo,
excluimos el archivo ISO:

```terminal
qemu-system-i386 -m 128 \
  -hda mandrake91.qcow2 -usb \
  -vga cirrus -soundhw ac97 \
  -nic user,model=rtl8139
```

Tras el [gestor de arranque][11] y algunos otros mensajes de arranque, podremos
ver la pantalla de acceso:

{% include postImage.html file="login-screen.png" alt="Pantalla de acceso de
Mandrake" width=1024 height=768 %}

Una vez hayamos accedido con nuestro usuario, se lanzará el *Asistente
Inicial* de Mandrake para que podamos elegir el [entorno de escritorio][6] a
usar ([KDE][17], [GNOME][12] o [IceWM][15]). He elegido KDE porque es el que he usaba
en esa época:

{% include postImage.html file="welcome.png" alt="Bienvenido a Mandrake"
width=1024 height=768 %}

Si abrimos una consola, podremos ver qué versión del núcleo de Linux estamos
corriendo con el comando `uname -a`:

```shell
Linux localhost 2.4.21-0.13mdk #1 Fri Mar 14 15:08:06 EST 2003 i686 unknown unknown GNU/Linux
```


### Ajustes

Para cambiar ajustes de escritorio como el fondo, el tema o los bordes de
ventana, simplemente tenemos que lanzar el *Centro de Control de KDE*:

{% include postImage.html file="kde-control-center.png" alt="Centro de Control
de KDE" width=1024 height=768 %}

Si queremos cambiar otros ajustes de sistema, sin embargo, deberemos ejecutar el
*Centro de Control de Mandrake*. Tras introducir nuestra contraseña de *root*,
podremos cambiar cosas como las opciones de arranque, los parámetros de red,
usuarios y ajustes de seguridad:

{% include postImage.html file="mandrake-control-center.png" alt="Centro de
Control de Mandrake" width=1024 height=768 %}


### Administrador de paquetes

Desde el *Centro de Control de Mandrake*, podemos también administrar los
paquetes de software usando *rpmdrake*. Esta herramienta gestiona
automáticamente las dependencias de forma que no nos veremos obligados a buscar
los paquetes requeridos por otro:

{% include postImage.html file="software-installation.png" alt="Instalación de
paquetes de software" width=1024 height=768 %}


### Gestor de archivos

El gestor de archivos es [Konqueror][18] que todavía sigue dando guerra hoy,
aunque sea solamente como navegador web por defecto de KDE:

{% include postImage.html file="konqueror-about.png" alt="Gestor de archivos"
width=1024 height=768 %}


### Navegador web

Desgraciadamente, como navegador web, esta versión de Konqueror no puede con
muchos de los sitios web modernos. Y tampoco el navegador web de [Mozilla][21]:

{% include postImage.html file="mozilla-about.png" alt="Navegador web de
Mozilla" width=1024 height=768 %}

De los sitios web que he probado, estos dos navegadores web sólo pudieron cargar
[Google][13]:

{% include postImage.html file="konqueror-google.png" alt="Google - Konqueror"
width=1024 height=768 %}

{% include postImage.html file="mozilla-google.png" alt="Google - Mozilla"
width=1024 height=768 %}

Ambos fueron incapaces de cargar [otros sitios web populares][28] como
[YouTube][33], [Facebook][10], [Wikipedia][30], [Yahoo][32], [Reddit][25],
[Twitter][29], [Amazon][1] o [Instagram][16].


### Herramientas ofimáticas

Me sorprendió el hecho de que [OpenOffice][23] ya existiera en el 2003, con sus
componentes habituales como Writer, Calc, Impress, etc.:

{% include postImage.html file="openoffice-about.png" alt="OpenOffice.org
Writer" width=1024 height=768 %}


### Multimedia

En lo que respecta a la reproducción multimedia, ha sido necesario instalar
[xine][31], puesto que ni [MPlayer][22] ni el visor por defecto de KDE pudieron
reproducir el [archivo AVI][4] de [Big Buck Bunny][3]:

{% include postImage.html file="video-player.png" alt="Reproductor de vídeo"
width=1024 height=768 %}

Aun así, la reproducción, tanto de audio como de vídeo, va a saltos. Quizás
los resultados mejorarían probando la emulación de otros dispositivos de audio
y vídeo.


## Conclusión

Me sorprendió gratamente la facilidad de uso de esta versión de Mandrake Linux,
incluso comparada con sistemas operativos modernos.

Las acciones de usuario más comunes del día a día, e incluso una gran parte de
la administración del sistema, se pueden realizar con algún tipo de [GUI][14],
sin renunciar al poder de la [shell de Unix][26] que se encuentra debajo.

Ciertamente, la mayoría de los problemas que nos habríamos encontrado en los
2000, en lo referente a Linux, hubiesen sido cuestiones de hardware y falta de
controladores. Afortunadamente, esta vez hemos podido evitar todo esto usando
una máquina virtual.

En cualquier caso, el esfuerzo invertido en crear semejante experiencia de
usuario es notable. Especialmente por el hecho de que sólo factores externos,
como el soporte por parte de los fabricantes, podrían evitar que disfrutásemos
de esta distribución en todo su esplendor.


[1]:  //web.archive.org/web/20170422/www.amazon.com/
[2]:  //web.archive.org/web/20170422/es.wikipedia.org/wiki/Internet_Archive
[3]:  //web.archive.org/web/20170422/peach.blender.org/
[4]:  //web.archive.org/web/20170422/download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_480p_stereo.avi
[5]:  //web.archive.org/web/20170422/www.linux-kvm.org/page/Change_cdrom
[6]:  //web.archive.org/web/20170422/es.wikipedia.org/wiki/Entorno_de_escritorio
[7]:  //web.archive.org/web/20170422/distrowatch.com/2699
[8]:  //web.archive.org/web/20170422/www.debian.org/
[9]:  //archive.org/details/Mandrake91
[10]: //web.archive.org/web/20170422/www.facebook.com/
[11]: //web.archive.org/web/20170422/es.wikipedia.org/wiki/Gestor_de_arranque
[12]: //web.archive.org/web/20170422/es.wikipedia.org/wiki/GNOME
[13]: //web.archive.org/web/20170422/www.google.com/
[14]: //web.archive.org/web/20170422/es.wikipedia.org/wiki/GUI
[15]: //web.archive.org/web/20170422/es.wikipedia.org/wiki/IceWM
[16]: //web.archive.org/web/20170422/www.instagram.com/
[17]: //web.archive.org/web/20170422/es.wikipedia.org/wiki/KDE
[18]: //web.archive.org/web/20170422/es.wikipedia.org/wiki/Konqueror
[19]: //web.archive.org/web/20170422/distrowatch.com/685
[20]: //web.archive.org/web/20170422/es.wikipedia.org/wiki/Mandriva
[21]: //web.archive.org/web/20170422/es.wikipedia.org/wiki/Mozilla_Suite
[22]: //web.archive.org/web/20170422/es.wikipedia.org/wiki/MPlayer
[23]: //web.archive.org/web/20170422/es.wikipedia.org/wiki/OpenOffice.org
[24]: //web.archive.org/web/20170422/www.qemu.org
[25]: //web.archive.org/web/20170422/www.reddit.com/
[26]: //web.archive.org/web/20170422/en.wikipedia.org/wiki/Unix_shell
[27]: //web.archive.org/web/20170422/opensource.com/article/16/12/yearbook-linux-test-driving-distros
[28]: //web.archive.org/web/20170422/www.alexa.com/topsites
[29]: //web.archive.org/web/20170422/twitter.com/
[30]: //web.archive.org/web/20170422/www.wikipedia.org/
[31]: //web.archive.org/web/20170422/es.wikipedia.org/wiki/Xine
[32]: //web.archive.org/web/20170422/www.yahoo.com/
[33]: //web.archive.org/web/20170422/www.youtube.com/
