---
layout:       post
title:        Thin client con Debian y xrdp
description:  Instala Debian 9 con Remmina para conectar con xrdp
lang:         es
ref:          debian-thinclient
categories:   blog
short:        ijUe
tags:         [linux, debian, thinclient, remote desktop]
---

{% include toc.md %}

Si sois como yo, probablemente tenéis al menos un viejo PC o portátil
acumulando polvo en algún lugar de vuestra casa. Hay muchas formas de [darle
vida a estos dispositivos][15], pero una opción muy sencilla es usarlos
como [*thin clients*][12] para acceder en remoto a una máquina más
potente y moderna.

<!--more-->

## Servidor

Vamos a configurar nuestro PC para que actúe como servidor. Asumiendo que
tenemos [Debian][21] instalado, solamente necesitaremos instalar el software de
servidor.


### xrdp

Para este objetivo, usaremos [xrdp][19] que es un servidor de código abierto
para el protocolo [RDP][8]. Para instalarlo, ejecutamos:

```terminal
sudo apt install xrdp
```


#### Asegurando la conexión

> Se usa Seguridad RDP Estándar, la cual no está a salvo de ataques de
> intermediario. El nivel de cifrado de la Seguridad RDP Estándar se controla
> con **crypt_level**.
>
> -- <cite markdown="1">[Manpages][20]</cite>

Definitivamente deberíamos hacer algo para remediar esto por lo que vamos a usar
[TLS][14] como capa de seguridad.

Los certificados necesarios se generan automáticamente durante la instalación
del paquete *ssl-cert* pero necesitaremos añadir el usuario *xrdp* a este grupo
para que pueda leer la clave privada:

```terminal
sudo adduser xrdp ssl-cert
```

Ahora vamos a editar el archivo `/etc/xrdp/xrdp.ini` y cambiar:

```ini
security_layer=negotiate
certificate=
key_file=
```

Por:

```ini
security_layer=tls
certificate=/etc/xrdp/cert.pem
key_file=/etc/xrdp/key.pem
```

Y reiniciamos el servicio:

```terminal
sudo service xrdp restart
```

Con esto, nuestro PC está preparado para recibir conexiones remotas desde el
*thin client*.


## Cliente

A continuación podemos proceder con la configuración de la máquina que actuará
como *thin client*.

Para ello necesitamos una instalación base de [Debian 9 (*stretch*)][1]
con apenas unos cuantos paquetes adicionales. Por lo tanto, deberemos
asegurarnos de que nada está marcado en el paso *Software selection* del
proceso de instalación:

{% include postImage.html file="software-selection.png" alt="Debian software
selection" width=800 height=600 %}

Posteriormente, una vez hayamos arrancado Debian, podemos instalar un [gestor de
pantalla][2] y un [gestor de ventanas][17] para tener un simple entorno
gráfico. Usaremos [LightDM][4] y [Openbox][7] respectivamente, y [tint2][13]
como barra de tareas liviana:

```terminal
sudo apt install lightdm openbox tint2 xterm
```

Tras un reinicio, podremos ver la pantalla de acceso:

{% include postImage.html file="debian-login.png" alt="Pantalla de acceso de
Debian" width=1024 height=768 %}

Vamos a configurar Openbox para que ejecute tint2 tras acceder al escritorio.
Necesitamos copiar la configuración por defecto de Openbox a nuestra carpeta de
usuario para poder modificarla:

```terminal
mkdir -p ~/.config/openbox && cp /etc/xdg/openbox/* ~/.config/openbox
```

Editaremos entonces el archivo `~/.config/openbox/autostart` para añadir, al
final, las siguientes líneas:

```shell
# Launch taskbar
tint2 &
```


### Remmina

Para acceder a nuestro servidor remotamente, usaremos [Remmina][9] puesto que
soporta varios protocolos ([RDP][8], [VNC][16], [SSH][11], [NX][6], [XDMCP][18],
etc.):

```terminal
sudo apt install remmina
```

Una vez ha finalizada la instalación, editaremos de nuevo el archivo `autostart`
de Openbox para ejecutar Remmina:

```shell
# Start Remmina
remmina &
```

Tras acceder a nuestro escritorio, podemos crear una nueva conexión de Remmina
pulsando **Ctrl+N**. Simplemente tenemos que poner un nombre para la conexión y
la dirección o nombre de equipo de nuestro servidor:

{% include postImage.html file="new-connection.png" alt="Nueva conexión de
Remmina" width=1024 height=768 %}

Una vez hagamos click sobre *Conectar*, se guardará la nueva conexión y se nos
pedirá que aceptemos el certificado TLS del servidor:

{% include postImage.html file="accept-cert.png" alt="Detalles del certificado"
width=1024 height=768 %}

Si lo aceptamos, veremos la pantalla de acceso de nuestro servidor remoto:

{% include postImage.html file="remote-login1.png" alt="Acceso remoto"
width=1024 height=768 %}

Aquí simplemente tenemos que introducir el nombre de usuario y contraseña de
nuestro servidor remoto y accederemos al escritorio. Si pulsamos **R_Ctrl+F**
cambiaremos a pantalla completa para una experiencia más transparente:

{% include postImage.html file="remote-desktop.png" alt="Escritorio remoto"
width=1024 height=768 %}


#### Mostrar la ventana de acceso remoto tras arrancar

Podemos configurar nuestro *thin client* Debian para que acceda al escritorio de
forma automática y Remmina lance la conexión a nuestro servidor de forma que se
nos presente directamente la pantalla de acceso remoto.

Primero deberemos configurar LightDM para que acceda automáticamente al
escritorio de nuestro usuario local. Para ello, tenemos que editar el archivo
`/etc/lightdm/lightdm.conf` como superusuario y configurar nuestro nombre de
usuario en la sección *Seat configuration*:

```ini
[Seat:*]
autologin-user=agus
```

Luego, debemos averiguar el nombre de archivo de nuestra conexión tal y como la
guarda Remmina. Las conexiones se guardan en `$HOME/.remmina`, para versiones
antiguas, o en `$XDG_DATA_HOME/remmina` para las más recientes.

```
~/.remmina
├── 1492192074855.remmina
└── remmina.pref
```

Conforme al nombre del archivo, tenemos que modificar la última línea del
archivo `autostart` de Openbox:

```shell
# Start Remmina
remmina -c ~/.remmina/1492192074855.remmina &
```

Si reiniciamos ahora, nos conectaremos directamente a la máquina remota y
veremos la pantalla de acceso:

{% include postImage.html file="remote-login2.png" alt="Acceso remoto"
width=1024 height=768 %}


#### Acceder al servidor remoto tras acceso en local

Podemos configurar Remmina para que guarde las credenciales del usuario remoto y
nos dé acceso al servidor automáticamente:

Para ello, necesitamos guardar el nombre de usuario y contraseña en el perfil de
la conexión:

{% include postImage.html file="save-credentials.png" alt="Guardar credenciales"
width=1024 height=768 %}

Sin embargo, para [mayor seguridad][10], deberíamos instalar el
complemento de GNOME para Remmina de forma que la contraseña se guarde en el
[llavero de GNOME][3]:

```terminal
sudo apt install remmina-plugin-gnome seahorse
```

Necesitaremos deslogarnos y acceder al escritorio de nuevo para que se genere el
llavero con nuestra contraseña local.

Ahora, si modificamos la conexión y añadimos las credenciales, la contraseña se
guardará en el llavero de GNOME:

{% include postImage.html file="gnome-keyring.png" alt="Llavero de GNOME"
width=1024 height=768 %}

Puesto que nuestra contraseña local es necesaria para desbloquear el llavero y
recuperar la contraseña remota, deberemos deshacer los cambios en
`/etc/lightdm/lightdm.conf`:

```ini
[Seat:*]
#autologin-user=agus
```

Si no, se nos pedirá desbloquear el llavero antes de que Remmina pueda
conectarse con nuestro servidor remoto.


### Poniendo orden

Puesto que esta máquina va a hacer poco más que ejecutar Remmina para
conectarnos a nuestro servidor, podemos desinstalar algunos paquetes superfluos.
Cosas como la administración de tareas o el registro de mensajes no tienen
sentido:

```terminal
sudo apt purge --auto-remove anacron cron rsyslog
```

Como nota adicional, si tenemos pensado conectarnos a una red inalámbrica,
quizás nos interese instalar [NetworkManager][5] y su applet para facilitarnos
las cosas:

```terminal
sudo apt install network-manager-gnome
```

Sin embargo, puest que NetworkManager usa el llavero de GNOME, no seremos
capaces de [acceder localmente de forma
automática](#mostrar-la-ventana-de-acceso-remoto-tras-arrancar) de forma
transparente.


## Conclusión

Como he visto, usar un viejo PC o portátil como *thin client* es una buena
manera de dar nueva vida a estos dispositivos.

Los requisitos de hardware son muy bajos puesto que usará principalmente la red.
Puedes ver la carga en un sistema con sólo 128 MB de RAM:

{% include postImage.html file="resource-usage.png" alt="Carga del sistema"
width=1024 height=768 %}


## Leer más

- [Problem found with Debian systems running systemd · Issue #190 ·
neutrinolabs/xrdp · GitHub][22]
- [TLS security layer · neutrinolabs/xrdp Wiki · GitHub][23]
- [Remmina Usage FAQ · FreeRDP/Remmina Wiki · GitHub][24]
- [Audio Output Virtual Channel support in xrdp · neutrinolabs/xrdp Wiki ·
  GitHub][25]
- [TransparentEncryptionForHomeFolder - Debian Wiki][26]
- [Linux Terminal Server Project - Welcome to LTSP.org][27]
- [ThinStation by Donald A. Cupp Jr.][27]


[1]:  //web.archive.org/web/20170415/www.debian.org/releases/stretch/index.es.html
[2]:  //web.archive.org/web/20170415/es.wikipedia.org/wiki/X_Display_Manager
[3]:  //web.archive.org/web/20170415/wiki.gnome.org/Projects/GnomeKeyring
[4]:  //web.archive.org/web/20170415/freedesktop.org/wiki/Software/LightDM/
[5]:  //web.archive.org/web/20170415/wiki.gnome.org/Projects/NetworkManager
[6]:  //web.archive.org/web/20170415/es.wikipedia.org/wiki/Tecnolog%C3%ADa_NX
[7]:  //web.archive.org/web/20170415/openbox.org/
[8]:  //web.archive.org/web/20170415/es.wikipedia.org/wiki/Remote_Desktop_Protocol
[9]:  //web.archive.org/web/20170415/www.remmina.org/
[10]: //web.archive.org/web/20170415/askubuntu.com/q/290824
[11]: //web.archive.org/web/20170415/es.wikipedia.org/wiki/Secure_Shell
[12]: //web.archive.org/web/20170415/es.wikipedia.org/wiki/Thin_client
[13]: //web.archive.org/web/20170415/gitlab.com/o9000/tint2
[14]: //web.archive.org/web/20170415/es.wikipedia.org/wiki/Transport_Layer_Security
[15]: //duckduckgo.com/?q=use+old+pc
[16]: //web.archive.org/web/20170415/es.wikipedia.org/wiki/VNC
[17]: //web.archive.org/web/20170415/es.wikipedia.org/wiki/Gestor_de_ventanas
[18]: //web.archive.org/web/20170415/es.wikipedia.org/wiki/XDMCP
[19]: //web.archive.org/web/20170415/www.xrdp.org/
[20]: //web.archive.org/web/20170415/manpages.debian.org/stretch/xrdp/xrdp.ini.5.en.html
[21]: //web.archive.org/web/20170415/www.debian.org/index.es.html
[22]: //web.archive.org/web/20170415/github.com/neutrinolabs/xrdp/issues/190
[23]: //web.archive.org/web/20170415/github.com/neutrinolabs/xrdp/wiki/TLS-security-layer
[24]: //web.archive.org/web/20170415/github.com/FreeRDP/Remmina/wiki/Remmina-Usage-FAQ
[25]: //web.archive.org/web/20170415/github.com/neutrinolabs/xrdp/wiki/Audio-Output-Virtual-Channel-support-in-xrdp
[26]: //web.archive.org/web/20170415/wiki.debian.org/TransparentEncryptionForHomeFolder
[27]: //web.archive.org/web/20170415/www.ltsp.org/
[27]: //web.archive.org/web/20170415/www.thinstation.org/
